#!/bin/bash

# get tpm2-tools tests
TPM2_TOOLS_VERSION=5.0
git clone https://github.com/01org/tpm2-tools.git
pushd tpm2-tools
git checkout -b test $TPM2_TOOLS_VERSION
git cherry-pick 6318507a8a2ca2db7423c939c399f71bee2d9d40 # test: add support for swtpm simulator
git cherry-pick 6f5b83ce758e4ed0bdfb591e8db2ee586cacae97 # test/testparms.sh: use algo that's not supported by swtpm for negative test
git cherry-pick e858dec76686bb4c42e74e0984b433231e530f93 # testparms: ensure curve not supported before negative test
git cherry-pick 0789bf264a108c4718875a050d00b1fdee4478b7 # testparms: fix condition for negative test
pushd test/integration
sed -i -e 's/\<python\>/python3/g' helpers.sh
pushd tests
sed -i -e 's/\<python\>/python3/g' *.sh
# some tests aren't executable currently. Needs to be fixed upstream.
chmod +x *.sh
popd
popd
popd

TPM2_ABRMD=tpm2-abrmd
TPM2_SIM=swtpm
TPM2_TOOLS_TEST_FIXTURES=`pwd`/tpm2-tools/test/integration/fixtures
PATH=`pwd`/ibmtpm/src/:.:$PATH
export TPM2_ABRMD TPM2_SIM TPM2_TOOLS_TEST_FIXTURES PATH

pushd tpm2-tools/test/integration
for t in `ls tests/*.sh`
do
    f=`basename $t`
    test=${f%%.*}
    /usr/share/automake-1.16/test-driver --test-name $test --log-file $test.log --trs-file $test.trs $t
done
all=`grep ":test-result:" *.trs | wc -l`;
pass=`grep ":test-result: PASS"  *.trs | wc -l`;
fail=`grep ":test-result: FAIL"  *.trs | wc -l`;
skip=`grep ":test-result: SKIP"  *.trs | wc -l`;
xfail=`grep ":test-result: XFAIL" *.trs | wc -l`;
xpass=`grep ":test-result: XPASS" *.trs | wc -l`;
error=`grep ":test-result: ERROR" *.trs | wc -l`;
if [[ $(($fail + $xpass + $error)) -eq 0 ]]; then
    success=0
else
    success=1
fi;
popd

echo PASSED: $pass
echo FAILED: $fail
echo SKIPPED: $skip
echo XFAIL: $xfail
echo XPASS: $xpass
echo ERROR: $error

exit $success
